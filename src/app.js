/**
 *  scrumplex.net clicklet
 *  Copyright (C) 2021 Sefa Eyeoglu <contact@scrumplex.net> (https://scrumplex.net)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

import "./app.scss"

const clicklet = document.getElementById("clicklet"),
    clickletLogo = clicklet.getElementsByClassName("clicklet-logo")[0];
let clickCount = 0;

clicklet.addEventListener("click", function () {
    clickCount++;
    if (clickCount >= 2) {
        window.open(clickletLogo.href);
        close();
    } else {
        open();
    }
});

document.addEventListener('click', function (e) {
    const elementMouseIsOver = document.elementFromPoint(e.clientX, e.clientY);

    if (elementMouseIsOver === clicklet)
        return;

    if (!isDescendant(elementMouseIsOver, clicklet))
        close();

});


clickletLogo.addEventListener("click", function (e) {
    e.preventDefault();
});

function open() {
    clicklet.classList.add("open");
}

function close() {
    clickCount = 0;
    clicklet.classList.remove("open");
}

function isDescendant(self, parent) {
    let node = self.parentNode;
    while (node != null) {
        if (node === parent) {
            return true;
        }
        node = node.parentNode;
    }
    return false;
}
