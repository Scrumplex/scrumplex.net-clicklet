scrumplex.net clicklet
----------------------
The scrumplex.net "clicklet" is a small advertisement on the bottom right corner of a website's viewport. 
It links to the creator of the website.

# Copy Paste
```html
<div class="clicklet" id="clicklet">
    <div class="clicklet-bg"><a href="https://scrumplex.net" class="clicklet-logo"></a>
        <div class="clicklet-text">
            This website was made with love by Scrumplex. Click anywhere on this box to learn more.
        </div>
    </div>
    <script src="https://static.scrumplex.net/clicklet.js" async></script>
</div>
```
```html
<div class="clicklet left" id="clicklet">
    <div class="clicklet-bg"><a href="https://scrumplex.net" class="clicklet-logo"></a>
        <div class="clicklet-text">
            This website was made with love by Scrumplex. Click anywhere on this box to learn more.
        </div>
    </div>
    <script src="https://static.scrumplex.net/js/clicklet.js" async></script>
</div>
```

# License
This project is licensed under the GNU General Public License v3.
You can find more information about it in the [LICENSE](LICENSE) file.
